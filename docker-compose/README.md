# How to Use

When testing out Postgres:

## Install Docker and Docker Compose

You have many options, please see the documentation linked about configuring [docker](https://gitlab.com/foss-contrib/gitlab-org/environments/gdk#install-docker) and [docker-compose](https://gitlab.com/foss-contrib/gitlab-org/environments/gdk#using-docker-compose) that exists on our GitLab Development Environment (don't follow instructions outside of docker and docker-compose headings; you're not configuring for GitLab Development - just installing Docker and Docker Compose ⇨ follow the links from those locations to get set up.)

## Install the Helper Script

For an easy method of fetching an interactive shell from a Docker container, you
can borrow the docker-helper.sh script provided in this repository if you are
using Linux (and possibly Mac):

* In a terminal change directories to bin
  * `cd ../bin`
* Mark the helper file as executable and alias (symbolically link) it in
  `/usr/local/bin`:
  ```bash
  chmod +x ./docker-helper.sh
  sudo ln -s /full/path/to/docker-helper.sh /usr/local/bin/docker-helper
  ```
* For any container you run, you can get either the ip for the container, or its
interactive shell, by providing the container's name to docker-helper as
follows:
  * For the interactive shell:
  ```bash
  docker-helper get-shell --name gdk-debian-slim
  ```
  * For the IP Address:
  ```bash
  docker-helper get-ip --name gdk-debian-slim
  ```
  * In the former case, you'll be brought into the context of the containers'
terminal - whereas in the latter the IP Address will be printed in the terminal.

## Using Docker Shell

Leverage the helper script:

```bash
docker-helper get-shell --name postgres-environment
```

Then log in using the right credentials, specifying the db if not the same as the username (as in our case):

```bash
psql -h localhost -U nodekit nodekit_test
```

## Using Adminer

Leverage Adminer, keeping in mind that localhost won't work for the database server name:

1. Visit http://localhost:7432/
2. Select `PostgreSQL`
3. For `Server`, set it to the default of `postgres`; don't set a port, it's `5432` by default - while adminer and postgres share a network within the docker runtime.
4. For `User`, set it to `nodekit`
5. For `db`, set it to `nodekit_test`
6. For `Password`, set it to `TestPass777`

Then click `Log In`, and you should be confronted with a successful login!

## Using Docker Compose

Change directory to `docker-comopse` and start the postgresql container, its already configured so that the connection configuration provided with this repository is accurate.

* If using docker-compose separately (installed following instructions from the GitHub repository for Docker Compose Plug-in):
  ```bash
  cd docker-compose
  docker-compose up -d
  ```
* If using Docker compose as provided by Docker Desktop:
  ```bash
  cd docker-compose
  docker compose up -d
  ```

You are now safe to run the tests. On GitLab, the proper images are already configured withint he GitLab-CI configuration; nothing extra is necessary