/**
 * SPDX-PackageName: kwaeri/postgresql-database-driver
 * SPDX-PackageVersion: 0.5.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 export default {
     "type": "postgresql",
     "host": "localhost",
     "port": 5432,
     "database": "nodekit_test",
     "user": "nodekit",
     "password": "TestPass777"
 }

export const test = {
    "type": "postgresql",
    "host": "postgres",
    "port": 5432,
    "database": "nodekit_test",
    "user": "nodekit",
    "password": "TestPass777"
}