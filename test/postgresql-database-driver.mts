/**
 * SPDX-PackageName: kwaeri/postgresql-database-driver
 * SPDX-PackageVersion: 0.5.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

// INCLUDES
import * as assert from 'assert';
import { PostgreSQLDriver } from '../src/postgresql-database-driver.mjs';
import { default as pgDefaultConfig, test } from '../data/db-config-postgresql.mjs';
import debug from 'debug';


// DEFINES
const DEBUG = debug( 'nodekit:postgresql-database-driver-test' );

let MIGRATION_ENV = process.env.NODE_ENV || "default";

const conf = {
    default: pgDefaultConfig,
    test: test
};

const pgdbo = new PostgreSQLDriver( ( conf as any )[MIGRATION_ENV] );
//let pgdriver = new PostgreSQLDriver( postgresqlConfig.default ),
    //pgdbo    = pgdriver.get();


// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {
        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );
    }
);


// Create table test
describe(
    'Postgres Functionality Test Suite',
    () => {

        /* Make sure to end the client pool only after we done with all tests */
        after(
            async () => {
                try {
                    await pgdbo.destroyPool();
                }
                catch( error ) {
                    DEBUG( `Error: ${error}` );

                    return Promise.reject( `${error}` );
                }

                return Promise.resolve( true );
            }
        );

        describe(
            'Create Table Test',
            () => {
                it(
                    'Should create the `people` table in the `nodekit_test` postgres database, and return the command processed (CREATE).',
                    async () => {
                        let results = null;

                        try {
                            // Get some results from the migrator:
                            results = await pgdbo
                            .query( `create table if not exists people ` +
                                    `( id serial primary key,` +
                                    `first_name varchar(255), ` +
                                    `last_name varchar(255), ` +
                                    `age int );` );

                            // Compare results to what we know the test should return:
                            const comparisonResult = ( ( results.rows as any ).command == 'CREATE' ) ? true : false;

                            DEBUG( `Command Processed: ${( results.rows as any ).command}` );

                            // Assert that the comparison is equivalent to 'true':
                            return Promise.resolve( assert.equal( true, comparisonResult ) );
                        }
                        catch( error ) {
                            console.log( `[ERROR]: ${error}` );

                            return Promise.reject( Error( `${error}` ) );
                        }
                    }
                );
            }
        );

        describe(
            'Insert Record Test',
            () => {
                it(
                    'Should insert a row into the new `people` table, returning the # of rows affected and insert id created.',
                    async () => {
                        let results = null;

                        try {
                            // Get some results from the migrator:
                            results = await pgdbo
                            .query( `insert into people ( first_name, last_name, age ) values('Richard', 'Winters', 35);` );

                            // Compare results to what we know the test should return:
                            const comparisonResult =  (
                                                    ( results.rows as any ).affectedRows == 1 &&
                                                    ( results.rows as any ).command     == 'INSERT'
                                                    ) ? true : false;

                            DEBUG( `Server Command: ${( results.rows as any ).command}, Affected Rows: ${( results.rows as any ).affectedRows}` );

                            // Assert that the comparison is equivalent to 'true':
                            return Promise.resolve( assert.equal( true, comparisonResult ) );
                        }
                        catch( error ) {
                            console.log( `[ERROR]: ${error}` );

                            return Promise.reject( Error( `${error}` ) );
                        }
                    }
                );
            }
        );


        describe(
            'Insert Record [with Placeholders] Test',
            () => {
                it(
                    'Should insert a row into the new `people` table, returning the # of rows affected and insert id created.',
                    async () => {
                        let results = null;

                        try {
                            // Get some results from the migrator:
                            results = await pgdbo
                            .query( `insert into people ( first_name, last_name, age ) values($1, $2, $3);`, [ "Richard", "Winters", 35] );

                            // Compare results to what we know the test should return:
                            const comparisonResult =  (
                                                    ( results.rows as any ).affectedRows == 1 &&
                                                    ( results.rows as any ).command     == 'INSERT'
                                                    ) ? true : false;

                            DEBUG( `Server Command: ${( results.rows as any ).command}, Affected Rows: ${( results.rows as any ).affectedRows}` );

                            // Assert that the comparison is equivalent to 'true':
                            return Promise.resolve( assert.equal( true, comparisonResult ) );
                        }
                        catch( error ) {
                            console.log( `[ERROR]: ${error}` );

                            return Promise.reject( Error( `${error}` ) );
                        }
                    }
                );
            }
        );


        describe(
            'Select Record Test',
            () => {
                it(
                    'Should return the column values of the first record inserted into the `people` table.',
                    async () => {
                        let results = null;

                        try {
                            // Get some results from the migrator:
                            results = await pgdbo
                            .query( `select * from people where id=1 and first_name='Richard' and last_name='Winters' and age=35;` );

                            // Compare results to what we know the test should return:
                            const comparisonResult =  (
                                                    ( results.rows as any )[0].id           == 1           &&
                                                    ( results.rows as any )[0].first_name   == 'Richard'   &&
                                                    ( results.rows as any )[0].last_name    == 'Winters'   &&
                                                    ( results.rows as any )[0].age          == 35
                                                    ) ? true : false;

                            DEBUG( `Returned record: Id: ${( results.rows as any )[0].id}` +
                                         `, First name: ${( results.rows as any )[0].first_name}` +
                                         `, Last name: ${( results.rows as any )[0].last_name}` +
                                         `, Age: ${( results.rows as any )[0].age}` );

                            // Assert that the comparison is equivalent to 'true':
                            return Promise.resolve( assert.equal( true, comparisonResult ) );
                        }
                        catch( error ) {
                            console.log( `[ERROR]: ${error}` );

                            return Promise.reject( Error( `${error}` ) );
                        }
                    }
                );
            }
        );


        describe(
            'Select Record [with Pooled Client and Parameters] Test',
            () => {
                it(
                    'Should return the column values of the second record inserted into the `people` table.',
                    async () => {
                        let results = null;

                        try {
                            // Get some results from the migrator:
                            results = await pgdbo
                            .clientQuery( `select * from people where id=$1 and first_name=$2 and last_name=$3 and age=$4;`, [2, "Richard", "Winters", 35] );

                            // Compare results to what we know the test should return:
                            const comparisonResult =  (
                                                    ( results.rows as any )[0].id           == 2           &&
                                                    ( results.rows as any )[0].first_name   == 'Richard'   &&
                                                    ( results.rows as any )[0].last_name    == 'Winters'   &&
                                                    ( results.rows as any )[0].age          == 35
                                                    ) ? true : false;

                            DEBUG( `Returned record: Id: ${( results.rows as any )[0].id}` +
                                         `, First name: ${( results.rows as any )[0].first_name}` +
                                         `, Last name: ${( results.rows as any )[0].last_name}` +
                                         `, Age: ${( results.rows as any )[0].age}` );

                            // Assert that the comparison is equivalent to 'true':
                            return Promise.resolve( assert.equal( true, comparisonResult ) );
                        }
                        catch( error ) {
                            console.log( `[ERROR]: ${error}` );

                            return Promise.reject( Error( `${error}` ) );
                        }
                    }
                );
            }
        );


        describe(
            'Delete Record Test',
            () => {
                it(
                    'Should delete both of the records previously inserted into `people` table, returning the # of rows affected (2).',
                    async () => {
                        let results = null;

                        try {
                            // Get some results from the migrator:
                            results = await pgdbo
                            .query( `delete from people where first_name='Richard' and last_name='Winters' and age=35;` );

                            // Compare results to what we know the test should return:
                            const comparisonResult =  ( ( results.rows as any ).affectedRows == 2 ) ? true : false;

                            DEBUG( `Affected Rows: ${( results.rows as any ).affectedRows}` );

                            // Assert that the comparison is equivalent to 'true':
                            return Promise.resolve( assert.equal( true, comparisonResult ) );
                        }
                        catch( error ) {
                            console.log( `[ERROR]: ${error}` );

                            return Promise.reject( Error( `${error}` ) );
                        }
                    }
                );
            }
        );


        describe(
            'Drop Table Test',
            () => {
                it(
                    'Should drop the `people` table from the `nodekit_test` postgres database, and return the command processed (DROP).',
                    async () => {
                        let results = null;

                        try {
                            // Get some results from the migrator:
                            results = await pgdbo
                            .query( `drop table people;` );

                            // Compare results to what we know the test should return:
                            const comparisonResult = ( ( results.rows as any ).command == 'DROP' ) ? true : false;

                            DEBUG( `Server Command: ${( results.rows as any ).command}` );

                            // Assert that the comparison is equivalent to 'true':
                            return Promise.resolve( assert.equal( true, comparisonResult ) );
                        }
                        catch( error ) {
                            console.log( `[ERROR]: ${error}` );

                            return Promise.reject( Error( `${error}` ) );
                        }
                    }
                );
            }
        );
    }
);
